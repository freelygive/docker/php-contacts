#!/bin/sh
set -e

# Create our main Drupal project.
echo "# Creating Drupal project"
rm -rf $DRUPAL_BUILD_ROOT
composer create-project drupal/recommended-project $DRUPAL_BUILD_ROOT
cd $DRUPAL_BUILD_ROOT

# Require the modules we want to ensure are supported.
echo "# Requiring Contacts"
composer config minimum-stability dev
composer require drupal/contacts

# Validate the requirements.
composer check-platform-reqs
