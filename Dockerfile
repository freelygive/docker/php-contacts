# Build from the specified PHP image.
ARG PHP_IMAGE
FROM php:${PHP_IMAGE}

# Use the specified configuration.
ARG PHP_INI_VER=production
RUN mv "$PHP_INI_DIR/php.ini-$PHP_INI_VER" "$PHP_INI_DIR/php.ini"

# Create a PHP-CLI configuration with no memory limit.
RUN cp "$PHP_INI_DIR/php.ini" "$PHP_INI_DIR/php-cli.ini" \
    && sed -i 's/memory_limit = .*/memory_limit = -1/' "$PHP_INI_DIR/php-cli.ini"

# Install additional php extensions and their dependencies.
RUN apt-get update && apt-get install -y \
        git \
        unzip \
        mariadb-client \
    && curl -sS https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions > /usr/local/bin/install-php-extensions \
    && chmod uga+x /usr/local/bin/install-php-extensions \
    && install-php-extensions zip gd mysqli pdo_mysql bcmath

# Install composer 1 and hirak/prestissimo.
RUN curl -sS https://getcomposer.org/installer | php -- --1 --install-dir=/usr/local/bin --filename=composer \
    && composer global require hirak/prestissimo
